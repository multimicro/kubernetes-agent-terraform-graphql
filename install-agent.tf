provider "kubernetes" {
  host  = var.kubernetes.api_endpoint
  cluster_ca_certificate = base64decode(var.kubernetes.cluster_ca_certificate)
  client_certificate = try(var.kubernetes.client_certificate, "") != "" ? base64decode(var.kubernetes.client_certificate) : null
  client_key = try(var.kubernetes.client_key, "") != "" ? base64decode(var.kubernetes.client_key) : null 
  token = try(var.kubernetes.token, null)
}

resource "kubernetes_namespace" "gitlab-agent" {
  metadata {
    annotations = var.annotations
    name = var.agent_namespace
  }
}

resource "kubernetes_secret" "gitlab-agent-token" {
  metadata {
    name      = "gitlab-agent-token"
    namespace = kubernetes_namespace.gitlab-agent.metadata[0].name
    annotations = var.annotations
  }

  data = {
    token = graphql_mutation.agent_token.computed_read_operation_variables.secret
  }
}

resource "kubernetes_service_account" "gitlab-agent" {
  metadata {
    name      = "gitlab-agent"
    namespace = kubernetes_namespace.gitlab-agent.metadata[0].name
    annotations = var.annotations
  }
}

resource "kubernetes_cluster_role" "cilium-alert-read" {
  metadata {
    name = "cilium-alert-read"
    annotations = var.annotations
  }

  rule {
    api_groups = ["cilium.io"]
    resources  = ["ciliumnetworkpolicies"]
    verbs      = ["list", "watch"]
  }
}

resource "kubernetes_cluster_role" "gitlab-agent-gitops-read-all" {
  metadata {
    name = "gitlab-agent-gitops-read-all"
    annotations = var.annotations
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_cluster_role" "gitlab-agent-gitops-write-all" {
  metadata {
    name = "gitlab-agent-gitops-write-all"
    annotations = var.annotations
  }

  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["create", "update", "delete", "patch"]
  }
}

resource "kubernetes_cluster_role_binding" "cilium-alert-read" {
  metadata {
    name = "cilium-alert-read"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.cilium-alert-read.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.gitlab-agent.metadata[0].name
    namespace = kubernetes_namespace.gitlab-agent.metadata[0].name
  }
}

resource "kubernetes_cluster_role_binding" "gitlab-agent-gitops-read-all" {
  metadata {
    name = "gitlab-agent-gitops-read-all"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.gitlab-agent-gitops-read-all.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.gitlab-agent.metadata[0].name
    namespace = kubernetes_namespace.gitlab-agent.metadata[0].name
  }
}

resource "kubernetes_cluster_role_binding" "gitlab-agent-gitops-write-all" {
  metadata {
    name = "gitlab-agent-gitops-write-all"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.gitlab-agent-gitops-write-all.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.gitlab-agent.metadata[0].name
    namespace = kubernetes_namespace.gitlab-agent.metadata[0].name
  }
}


resource "kubernetes_deployment" "gitlab-agent" {
  wait_for_rollout = false
  metadata {
    name      = "gitlab-agent"
    namespace = kubernetes_namespace.gitlab-agent.metadata[0].name
    annotations = var.annotations
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "gitlab-agent"
      }
    }

    template {
      metadata {
        labels = {
          app = "gitlab-agent"
        }
        annotations = var.annotations
      }

      spec {
        service_account_name = kubernetes_service_account.gitlab-agent.metadata[0].name
        container {
          image = "registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/agentk:${var.agent_version}"
          name  = "agent"
          args = [
            "--token-file=/config/token",
            "--kas-address",
            "wss://kas.gitlab.com",
          ]
          volume_mount {
            name       = "token-volume"
            mount_path = "/config"
          }
        }

        volume {
          name = "token-volume"
          secret {
            secret_name = kubernetes_secret.gitlab-agent-token.metadata.0.name
          }
        }
      }
    }
    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_surge       = 0
        max_unavailable = 1
      }
    }
  }
}
