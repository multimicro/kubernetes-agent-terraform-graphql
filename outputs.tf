output "token_secret" {
  value     = graphql_mutation.agent_token.computed_read_operation_variables.secret
  sensitive = true
}

output "agent_id" {
  value = graphql_mutation.cluster_agent.computed_read_operation_variables.id
}
