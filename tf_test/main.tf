module "gitlab_kubernetes_agent" {
  source = "../"

  gitlab_project_id      = var.gitlab_project_id
  gitlab_username        = var.gitlab_username
  gitlab_password        = var.gitlab_password
  gitlab_graphql_api_url = var.gitlab_graphql_api_url
  agent_name             = var.agent_name
  token_name             = var.token_name
  token_description      = var.token_description
}

output "agent_id" {
  value = module.gitlab_kubernetes_agent.agent_id
}
